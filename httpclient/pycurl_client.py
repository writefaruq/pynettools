""" A HTTP client wrapped around PyCURL library """

import re
import pycurl
import cStringIO
import logging

import  utils.client_exceptions as client_exceptions

logger = logging.getLogger(__name__)

PYCURL_DEBUG_FILE = 'pycurl_debug.log'

def debug_callback(debug_type, debug_msg):
    """
    A defult pycurl debug callback write to a debug file
    
    Available curl_infotype values: 
        CURLINFO_TEXT: The data is informational text. 
        CURLINFO_HEADER_IN: The data is header (or header-like) data received from the peer.
        CURLINFO_HEADER_OUT: The data is header (or header-like) data sent to the peer.
        CURLINFO_DATA_IN: The data is protocol data received from the peer.
        CURLINFO_DATA_OUT: The data is protocol data sent to the peer.
    """
    # what to log
    # These type names are assumed
    (CURLINFO_TEXT, CURLINFO_HEADER_IN, CURLINFO_HEADER_OUT,\
     CURLINFO_DATA_IN, CURLINFO_DATA_OUT, CURLINFO_SSL_DATA_IN, CURLINFO_SSL_DATA_OUT)= range(7)
    if debug_type not in [CURLINFO_TEXT, CURLINFO_HEADER_IN, CURLINFO_HEADER_OUT]:
        return    
    msg = "HTTPClient pyCURL debug(%d): %s\n" % (debug_type, debug_msg)
    out = open(PYCURL_DEBUG_FILE, 'ab')
    out.write(msg)
    out.close()


class PyCurlClient(object):
    """ 
    Makes HTTP GET request via PyCurl over SSL and via Proxy
    
    Stores HTTP response results
    
    """    
    def __init__(self, debug=1, connection_timeout=10, 
                 max_redirs=3, timeout=20, no_signal=1,
                 debug_callback=debug_callback):
        self.curl = pycurl.Curl()
        self.buf = cStringIO.StringIO()
        self.verbose = int(debug)
        self.connection_timeout = connection_timeout
        self.max_redirs = max_redirs
        self.timeout = timeout
        self.no_signal = no_signal
        self.curl_options = {}
        self.debug_callback = debug_callback
        self.curl_perfomed = False
    
    def _setopt(self, key, value):
        """ 
        set curl opt and
        Store the already set curl options as there is noway found to do this using pycurl
        """
        try:
            self.curl_options[key] = value
            self.curl.setopt(key, value)
        except Exception:
            msg = "Failed to set option: key=%s val=%s" %(str(key), str(value))
            raise client_exceptions.PyCurlException(msg)

    def _set_curl_defaults(self):
        self.curl_perfomed = False
        self._setopt(pycurl.WRITEFUNCTION,self.buf.write)        
        # Error Control options (optional)
        self._setopt(pycurl.MAXREDIRS, self.max_redirs)
        self._setopt(pycurl.CONNECTTIMEOUT, self.connection_timeout)
        self._setopt(pycurl.TIMEOUT, self.timeout)
        self._setopt(pycurl.NOSIGNAL, self.no_signal)
        # debug_callback
        self._setopt(pycurl.VERBOSE, self.verbose)
        self._setopt(pycurl.DEBUGFUNCTION, self.debug_callback)
    
    def set_curl_option(self, url=None, proxy=None, proxy_user=None, ssl_cert=None, 
                        ssl_cert_passwd=None, **kwargs):
        """ 
        Sets curl option to Curl object and stores a reference to curl_options dict 
        You can pass any valid pyCURL option via kwargs dict
        """
        # Enforce user to set url before any other option
        if not (self.curl_options.has_key(pycurl.URL)):
            if not url:
                raise Exception("URL must be set before any other option")
            else:
                self._setopt(pycurl.URL, url)
                self._set_curl_defaults()
        # proxy setup
        if proxy and proxy_user:
            self._setopt(pycurl.PROXYAUTH, 255)
            self._setopt(pycurl.PROXY, proxy)
            self._setopt(pycurl.PROXYUSERPWD, proxy_user)
        # ssl setup
        if ssl_cert and ssl_cert_passwd:
            self._setopt(pycurl.SSL_VERIFYPEER,0)
            self._setopt(pycurl.SSLCERTTYPE,'P12')
            self._setopt(pycurl.SSLVERSION,3)
            self._setopt(pycurl.SSLCERT, ssl_cert)
            self._setopt(pycurl.SSLCERTPASSWD, ssl_cert_passwd)
        # handle kwargs
        for (key, value) in kwargs.iteritems():
            self._setopt(key, value)
        logger.debug("Set up curl options: %s" %self.curl_options)
            
   
    def get_curl_option(self, key):
        """
        Gets the value of a given option
        """
        if self.curl_options.has_key(key):
            return self.curl_options[key] 

    def run(self):
        """
        Calls the curl.perform()
        """
        try:
            self.curl.perform()            
        except Exception as e:
            raise client_exceptions.ConnectionFailedException("Curl Failed: %s" %str(e))
        self.curl_perfomed = True 
    
    def get_result(self):
        """
        returns the HTTP response data 
        """
        return self.buf.getvalue()

    def get_connection_stat(self):
        """
        Gives connection statistics
        """
        if self.curl_perfomed:
            return '\n'.join([
                    "HTTP-code: %s" %self.curl.getinfo(pycurl.HTTP_CODE),
                    "Total-time: %s" %self.curl.getinfo(pycurl.TOTAL_TIME),
            ])

    
    def get_download_stat(self):
        """
        Gives connection statistics
        """
        if self.curl_perfomed:
            return '\n'.join([                  
                    "Download speed: %.2f bytes/second" %self.curl.getinfo(pycurl.SPEED_DOWNLOAD),
                    "Document size: %d bytes" %self.curl.getinfo(pycurl.SIZE_DOWNLOAD)
            ])
    
    def get_upload_stat(self):
        """
        Gives  upload statistics
        """
        if self.curl_perfomed:
            return '\n'.join([
                            "HTTP-code: %s" %self.curl.getinfo(pycurl.HTTP_CODE),
                            "Total-time: %s" %self.curl.getinfo(pycurl.TOTAL_TIME),
                            "Upload speed: %.2f bytes/second" %self.curl.getinfo(pycurl.SPEED_UPLOAD),
                            "Document size: %d bytes" %self.curl.getinfo(pycurl.SIZE_UPLOAD)
                            ])
