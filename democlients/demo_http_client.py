"""Demo http client using PyCurlClient"""

import sys

from httpclient.pycurl_client import PyCurlClient

from utils.config_loader import CombinedConfigLoader

def demo_client_main():
    config_loader = CombinedConfigLoader(cmdline_args=sys.argv[1:])
    config = config_loader.get_config()
    client = PyCurlClient()
    client.set_curl_option(url=config.url)
    client.run()
    print client.get_result()

if __name__ == '__main__':
    demo_client_main()
    
