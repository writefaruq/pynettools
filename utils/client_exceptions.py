"""A bunch of exceptions and logging"""

import logging

logger =  logging.getLogger(__name__)


class HTTPClientException(Exception):
    pass


class ConnectionFailedException(HTTPClientException):
    """ When connection failed"""
    
    def __init__(self, msg):
        super(ConnectionFailedException, self).__init__()
        logger.critical("Connection failed: %s" %msg)


class UploadFailedException(HTTPClientException):
    """ If upload fails should we retry?"""
    
    def __init__(self, msg):
        super(UploadFailedException, self).__init__()
        logger.critical("Upload failed: %s" %msg)


class ConfigFileException(HTTPClientException):
    """ If something wrong in config file """
    
    def __init__(self, msg):
        super(ConfigFileException, self).__init__()
        logger.error("Config Error: %s" %msg)

class PyCurlException(HTTPClientException):
    """ If something wrong in config file """
    
    def __init__(self, msg):
        super(PyCurlException, self).__init__()
        logger.error("PyCurl Error: %s" %msg)

