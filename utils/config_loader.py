""" Implements configuration parsing from config_file and command_line"""

import argparse
import ConfigParser

import utils.client_exceptions as client_exceptions


class ConfigFileLoader(object):
    """ 
    Loads config file from file and conveniently set attributes to itself
    
    Save config data to a given config_file 
    """
    def __init__(self):
        self.config_parser = ConfigParser.SafeConfigParser()
        # config options 
        self.use_proxy = 0
        self.proxy = ''
        self.proxy_user =  ''
        self.use_cert = 0
        self.cert_file = ''
        self.debug   = 1
        self.connection_timeout = 10
        self.url = None

    def parse_config_file(self, config_file, target_config=None):  
        """Parse the config file and set the config options to target_config attributes"""
        if not target_config:
            target_config = self                 
        try:
            self.config_parser.read(config_file)
        except Exception as e:
            raise client_exceptions.ConfigFileException("Test config file: %s not found: %s " %(config_file, str(e)))
        for section in self.config_parser.sections():
            items = self.config_parser.items(section)
            for (name, value) in items:  
                setattr(target_config, name, value)
    
    def print_options(self):
        print "[proxy]"
        print 'use_proxy = ',str(self.use_proxy)
        print 'proxy = ',str(self.proxy)
        print 'proxy_user = ',str(self.proxy_user)
        print "[cert]"
        print 'use_cert = ',str(self.use_cert)
        print 'cert_file = ',str(self.cert_file)
        print "[general]"
        print 'debug = ',str(self.debug)
        print "connection_timeout = ",self.connection_timeout
 
       
    def save_config_file(self, config_file):
        """Save config data to config_file"""
        # try adding sections if not exist
        try:
            for section in ['proxy', 'cert', 'general']:
                self.config_parser.add_section(section)
        except ConfigParser.DuplicateSectionError:
            pass
        except Exception:
            raise
        self.use_proxy = 1 if self.proxy else 0
        self.config_parser.set('proxy', 'use_proxy', str(self.use_proxy))
        self.config_parser.set('proxy', 'proxy', str(self.proxy))
        self.config_parser.set('proxy', 'proxy_user', str(self.proxy_user))
        # cert
        self.use_cert = 1 if self.cert_file else 0
        self.config_parser.set('cert', 'use_cert', str(self.use_cert))
        self.config_parser.set('cert', 'cert_file', str(self.cert_file))
        # general
        self.config_parser.set('general', 'debug', str(self.debug))
        self.config_parser.set('general', 'connection_timeout', str(self.connection_timeout))
        with open(config_file, 'w') as config_file:
            self.config_parser.write(config_file)


class CombinedConfigLoader(object):
    """
    Combines config info from command line and config file.
    
    Args are mutually exclusive in cmdline and config file
    """
    def __init__(self, cmdline_args):
        self.arg_parser = argparse.ArgumentParser(description='')
        self._add_cmdline_args()        
        self.config = self.arg_parser.parse_args(cmdline_args) # default is sys.argv[1:]
        if self.config.config_file:
            self._combine_cfg_file_args()
    
    def get_config(self):
        return self.config
    
    def _combine_cfg_file_args(self):
        """Merge config file args with commandline args"""
        config_loader = ConfigFileLoader()
        config_loader.parse_config_file(self.config.config_file, self.config)
    
    def _add_cmdline_args(self):
        """Define all cmdline args here"""
        self.arg_parser.add_argument('--config-file', 
                                     dest = 'config_file', 
                                     help = 'A config file path containing configuration data.')
        self.arg_parser.add_argument('--debug', 
                                     dest = 'debug', 
                                     action = 'store_true', 
                                     help ='Set it verbose')
        self.arg_parser.add_argument('--url', 
                                     dest = 'url', 
                                     action = 'store', 
                                     help ='Make connection to this URL')
       
if __name__ == '__main__':
    import sys
    cfg = CombinedConfigLoader().get_config()
    print cfg.config_file